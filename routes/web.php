<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\LikeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::post('like_toggle', [LikeController::class, 'toggle_like'])->middleware('auth');


Route::get('index', [PostController::class, 'index'])->name('post-index')->middleware('auth');
Route::post('index', [PostController::class, 'create'])->name('post-create')->middleware('auth');
Route::get('post-store', [PostController::class, 'store'])->name('post-store')->middleware('auth');
Route::get('comment-index', [PostController::class, 'comment'])->name('comment-create')->middleware('auth');
Route::post('comment-index', [PostController::class, 'comment'])->name('comment-create')->middleware('auth');

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
