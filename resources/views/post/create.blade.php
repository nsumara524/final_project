<form method="POST" action="{{ route('post-create') }}" enctype="multipart/form-data" >
    @csrf

    <div class="row mb-3">
        <div class="col-12">
            <input  id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Title" value="{{ old('title') }}" required autocomplete="title" autofocus>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <textarea  id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description"  placeholder="Description" value="{{ old('description') }}" required autocomplete="description"></textarea>
        </div>
    </div>
    <div class="row mb-3">

        <div class="col-12">
            <input  id="avatar" type="file" class="form-control @error('avatar') is-invalid @enderror" name="avatar"  value="{{ old('avatar') }}" required autocomplete="avatar">

        </div>
    </div>
    <div class="row mb-0">
        <div class="col-12 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Post') }}
            </button>
        </div>
    </div>

</form>
