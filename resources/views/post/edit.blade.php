  {{-- commentEdit form  --}}
<form action="{{ route('commenteditroute',$comment->id) }}" method="POST">
   @csrf
    <div class="form-floating">
        <textarea class="form-control " name="comment"  placeholder="Leave a comment here" id="floatingTextarea">{!! $comment->comment !!}</textarea>
        <button class="btn btn-success btn-sm mt-4">Update</button>
    </div>
    <span class="input alert_denger">
        @error('comment')
            {{ $message }}
        @enderror
    </span>
    </form>
