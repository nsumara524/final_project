
@extends('layouts.app')
@section('content')



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Post Your Question</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @include('post.create')
            </div>

        </div>
    </div>
</div>




<div class="container">
    <div class="row" >
        @if(auth()->check())
        <div class="col-12 p-5 text-center">
            <button class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal">Ask a Question</button>
        </div>
        @else
        <h4 class="text-center">Want to Ask a question? Please <a href="{{ url('login') }}">Login</a></h4>
        @endif
        @foreach($post as $item)

        <div class="offset-3 col-6 mb-4 post-wrapper" data-post="{{ $item['id']}}">
            <div class="card">
                <div class="card-body">
                    <p>
                        <div>
                            @if(@$item['user'])
                            <img class="pic" src="{{ asset('storage/'.$item['user']['avatar']) }}" class="img-responsive" style="max-width:50px"  alt="pic.jpg ">
                            {{ $item['user']['name'] }}
                            @endif
                        </div>
                    </p>
                    <h5 class="card-title">{{$item['title']}}</h5>
                    <img class="pic" src="{{ asset('storage/'.$item['avatar']) }}" class="img-responsive" style="width:100%"  alt="pic.jpg ">
                    <p class="card-text">{{$item['description']}}</p>
                    <p class="card-text">
                        <small class="text-muted">Last updated {{ \Carbon\Carbon::createFromTimeStamp(strtotime($item['created_at']))->diffForHumans() }}</small>
                    </p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-6">
                            @if(auth()->check() )
                            @if(@count(@$item['likes'])>0)
                            <i class="fa-solid fa-thumbs-up text-info like-toggle" role='button'></i>
                            @else
                            <i class="fa-solid fa-thumbs-up like-toggle" role='button'></i>
                            @endif
                            @else
                            <a href="{{ url('login') }}" class="text-dark"><i class="fa-solid fa-thumbs-up" role='button'></i></a>
                            @endif
                            <i class="fa-solid fa-comments toggle_commentbox" role='button'></i>
                        </div>
                        <div class="col-6 text-center">
                            <a href="" class="toggle_commentbox" >{{ $item['comments_count']}} comments</a>
                        </div>
                    </div>
                    <div class="commentbox row m-5 bordered"  style="display: none">
                        @if(@$item['comments'])
                        @foreach($item['comments'] as $comment)
                        <div class="col-12 mb-2 border-bottom">
                            <div class="d-flex">
                                <p>
                                    <div>
                                        @if(@$comment['user'])
                                        <img class="pic" src="{{ asset('storage/'.$comment['user']['avatar']) }}" class="img-responsive" style="max-width:50px"  alt="pic.jpg ">
                                        {{ $comment['user']['name'] }}<br>
                                        <small class="text-muted">{{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment['created_at']))->diffForHumans() }}</small>
                                        @endif
                                    </div>
                                </p>
                            </div>
                            <span class="pl-3">{{$comment['comment']}}</span>

                        </div>
                        @endforeach
                        @endif
                        <div class="col-12 mb-2" >
                            @if(auth()->check())
                            <form action="{{ url('comment/create') }}" method="post">
                                <input type="hidden" name="post_id" value="{{ $item['id'] }}">
                                <div class="d-flex">
                                    <input type="text" name="comment">
                                    <button type="submit" class="btn btn-primary">Comment</button>
                                </div>
                            </form>
                            @else
                            <h5 class="mb-0"> <a href="{{ url('login') }}"> LOGIN TO COMMENT </a></h5>
                            @endif
                        </div>

                    </div>

                </div>
            </div>

        </div>
        @endforeach
    </div>
</div>
@endsection
