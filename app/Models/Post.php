<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }
    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }
    public function likes(){
        return $this->hasMany(PostLike::class, 'created_by');
    }
}
