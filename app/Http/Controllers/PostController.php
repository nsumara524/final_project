<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::orderBy('id', 'DESC')->get();
        return view('post.index', compact('post'));
    }
    public function create(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'title' => 'required|min:3',
            'description' => 'required',
            'avatar' => 'required',
        ]);
        if ($request->hasFile('avatar')) {
            $path = $request->file('avatar');
            $target = 'public/image';
            $file = Storage::putfile($target, $path);
            $file = substr($file, 7, strlen($file) - 7);
        }
        Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'avatar' => $file,
            'created_by' => auth()->user()->id
        ]);
        return redirect(url('/'));
    }


    public function comment(Request $request)
    {
        // dd($request);
        $request->validate(['comment' => "required"]);
        Comment::create([
            'comment' => $request->comment,
            'post_id' => $request->post_id,
            'created_by' => auth()->user()->id
        ]);
        return back();
    }
    public function showcomment()
    {
        $post = Post::orderBy('id', 'DESC')->get();
        return view('post.index', compact('post'));
    }
}
