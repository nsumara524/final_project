<?php

namespace App\Http\Controllers;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rels = ['user','comments.user'];
        if(auth()->check()){
            $rels['likes'] = function($q){
                $q->where('created_by', auth()->user()->id);
            };
        }
        $post = Post::with($rels)->withCount('comments','likes')->orderBy('created_at', 'DESC')->get()->toArray();
        return view('post.front', compact('post'));
    }
}
