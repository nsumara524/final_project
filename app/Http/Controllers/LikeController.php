<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PostLike;

class LikeController extends Controller
{
    public function toggle_like(Request $request){
        $data = $request->all();
        $res = [];
        if(@$data['post_id']){
            $user_id = auth()->user()->id;
            $check = PostLike::where('post_id',$data['post_id'])->where('created_by',$user_id)->count();
            if($check>0){
                PostLike::where('post_id',$data['post_id'])->where('created_by',$user_id)->delete();
                $res['check'] = 'disliked';
            }
            else{
                $insert_data = [
                    'post_id' => $data['post_id'],
                    'created_by' => $user_id
                ];
                PostLike::create($insert_data);
                $res['check'] = 'liked';
            }
        }
        return $res;
    }
}
