<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role' => 'admin',
        ])->each(function($user){
            Post::factory(10)->create()->each(function($post){
                Comment::factory(5)->create([
                    'post_id' => $post->id
                ])->each(function($comment){

                });
            });
        });

        // \App\Models\User::factory(10)->create();
    }
}
